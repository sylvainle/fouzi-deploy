# install

```bash
# $ kubectl rollout restart -nprometheus statefulset.apps/prometheus
$ kubectl port-forward svc/prometheus-operated -nprometheus 9090:9090
```

- [http://localhost:9090/]
- rate(healthcheck_total[5m])
